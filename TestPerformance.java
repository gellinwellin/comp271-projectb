package cs271.lab.list;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPerformance {

	// TODO run test and record running times for SIZE = 10, 100, 1000, 10000
	// which of the two lists performs better as the size increases?
	private final int SIZE = 10;

	private final int REPS = 1000000;

	private List<Integer> arrayList;

	private List<Integer> linkedList;

	private long current;
	private long end; 
	private long total;


	public void setUp() throws Exception {
		arrayList = new ArrayList<Integer>(SIZE);
		linkedList = new LinkedList<Integer>();
		for (int i = 0; i < SIZE; i++) {
			arrayList.add(i);
			linkedList.add(i);

		}
	}
	
	@After
	public void tearDown() throws Exception {
		arrayList = null;
		linkedList = null;
	}

	@Test
	public void testLinkedListAddRemove() {
		long current = System.currentTimeMillis();
		for (int r = 0; r < REPS; r++) {
			linkedList.add(0, 77);
			linkedList.remove(0);
		}
		long end = System.currentTimeMillis();
		long total = end - current;
	}

	@Test
	public void testArrayListAddRemove() {
		long current = System.currentTimeMillis();
		for (int r = 0; r < REPS; r++) {
			arrayList.add(0, 77);
			arrayList.remove(0);
		}
		long end = System.currentTimeMillis();
		long total = end - current;
		System.out.println("AddRemove ArrayList = " + total);
	}

	@Test
	public void testLinkedListAccess() {
		long current = System.currentTimeMillis();
		long sum = 0;
		for (int r = 0; r < REPS; r++) {
			sum += linkedList.get(r % SIZE);
				}
		long end = System.currentTimeMillis();
		long total = end - current;
		System.out.println("AddRemove LinkedList = " + total);

	}
	

	@Test
	public void testArrayListAccess() {
		long current = System.currentTimeMillis();
		long sum = 0;
		for (int r = 0; r < REPS; r++) {
			sum += arrayList.get(r % SIZE);
		}
		long end = System.currentTimeMillis();
		long total = end - current;
		System.out.println("testArrayAccess =" + total);
	}
	
}
