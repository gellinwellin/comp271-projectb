##This is the summary document for project0b, homework 1 for COMP 414. 
	
## The code in this repo is the following
 *Testlist.java
 *Testiterator.java
 *TestPerformance.java

 TestList

*The list does not make a difference whether it's a List or LinkedList. All tests still pass successfully upon changing to a LinkedList.

testRemoveObject()
*The method list.remove(5) removes the integer at the 5th index of the List, not remove the value 5 from the list. list.remove(ValueOf(5)) removes the integer within the list which has the value of 5. 

 Test Iterator

*A linkedList does not make a difference compared to the ArrayList for the iterator. Since the code is executing methods which occur incrementally from the beginning, both perform at the same level. However, if it were to be performing random insertions and deletions, the LinkedList would perform worse since the structure of a LinkedList makes insertions and deletions more difficult. 

 Test Performance

*The Arraylist performs better since it does not have to start at the beginning of the list to search for values which it would like to remove and/or add. 





